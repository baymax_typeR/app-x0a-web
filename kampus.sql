-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Apr 2020 pada 15.33
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kampus`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` varchar(16) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `tgllahir` varchar(50) NOT NULL,
  `gender` varchar(25) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `id_prodi` int(11) NOT NULL,
  `photos` varchar(24) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `nama`, `tgllahir`, `gender`, `alamat`, `id_prodi`, `photos`) VALUES
('1831001', 'Kakashi', '10 - 10 -1965', 'Laki - Laki', 'Jalan Mawar No 01', 1, 'kakashi.jpg'),
('1832001', 'Obito', '05 - 03 - 1978', 'Laki - Laki', 'Jalan Kembang No 10', 2, 'obito.jpg'),
('1833001', 'Rin', '07 - 08 - 1990', 'Perempuan', 'Jalan Teratai No 07', 3, 'rin.jpg'),
('1931001', 'Naruto', '12 - 05 - 1997', 'Laki - Laki', 'Jalan Teratai No 05', 1, 'naruto.jpg'),
('1932001', 'Sasuke', '07 - 03 -1987', 'Laki - Laki', 'Jalan Kenanga No 10', 2, 'sasuke.jpg'),
('1933001', 'Sakura', '01 - 09 - 1982', 'Perempuan', 'Jalan Hatta No 10', 3, 'sakura.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `prodi`
--

CREATE TABLE `prodi` (
  `id_prodi` int(11) NOT NULL,
  `nama_prodi` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `prodi`
--

INSERT INTO `prodi` (`id_prodi`, `nama_prodi`) VALUES
(1, 'Manajemen Informatika'),
(2, 'Mesin'),
(3, 'Akuntansi');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim`);

--
-- Indeks untuk tabel `prodi`
--
ALTER TABLE `prodi`
  ADD KEY `id_prodi` (`id_prodi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `prodi`
--
ALTER TABLE `prodi`
  MODIFY `id_prodi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
