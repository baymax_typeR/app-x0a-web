<?php
    $DB_NAME = "kampus";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $nama_prodi = $_POST['nama_prodi'];
        $sql = "select id_prodi, nama_prodi
                from prodi where nama_prodi like '%$nama_prodi%'";
        $result = mysqli_query($conn, $sql);
        $khoi = mysqli_num_rows($result);
        if( $khoi > 0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_mhs = array();
            while ($mhs = mysqli_fetch_assoc($result)) {
                array_push($data_mhs, $mhs);
            }
            echo json_encode($data_mhs);
        }
    }
 ?>
